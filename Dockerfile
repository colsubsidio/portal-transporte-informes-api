FROM openjdk:11.0-jdk-slim-stretch
ARG DEPENDENCY=target
COPY ${DEPENDENCY}/app-ryt-portal-transporte-informes-api-0.0.1-SNAPSHOT.jar /home/app-ryt-portal-transporte-informes-api-0.0.1-SNAPSHOT.jar

RUN echo $ENVIROMENTS
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=release", "-Duser.timezone=America/Bogota","/home/app-ryt-portal-transporte-informes-api-0.0.1-SNAPSHOT.jar"]
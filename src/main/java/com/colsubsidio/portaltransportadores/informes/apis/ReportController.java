package com.colsubsidio.portaltransportadores.informes.apis;

import com.colsubsidio.portaltransportadores.informes.business.ReportBusiness;
import com.colsubsidio.portaltransportadores.informes.commons.dto.ResponseDemandList;
import com.colsubsidio.portaltransportadores.informes.commons.dto.ResponseReportList;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("informes" )
@AllArgsConstructor( onConstructor = @__(@Inject) )
public class ReportController {

    private final @NonNull ReportBusiness reportBusiness;

    @ApiOperation(value = "Reporte de liquidación de transporte por empresa", produces = MediaType.APPLICATION_JSON_VALUE, response = ResponseReportList.class)
    @GetMapping(value = "/liquidacion", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseReportList> obtainSettlement(
            @RequestParam(name = "nitEmpresa", required=false) String nitBusiness,
            @RequestParam(name = "puntoTransporte", required=false) String transportationPoint,
            @RequestParam(name = "fechaInicial", required=false) String initialDate,
            @RequestParam(name = "fechaFinal", required=false) String finalDate) {
        return new ResponseEntity<>(this.reportBusiness.obtain( nitBusiness, transportationPoint, initialDate, finalDate ), HttpStatus.OK);
    }

    @ApiOperation(value = "Reporte de transacciones de transporte", produces = MediaType.APPLICATION_JSON_VALUE, response = ResponseReportList.class)
    @GetMapping(value = "/transacciones", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseReportList> obtainTransactions (
            @RequestParam(name = "nitEmpresa", required=false) String nitBusiness,
            @RequestParam(name = "fechaInicial", required=false) String initialDate,
            @RequestParam(name = "fechaFinal", required=false) String finalDate) {
        return new ResponseEntity<>(this.reportBusiness.obtain( nitBusiness, initialDate, finalDate ), HttpStatus.OK);
    }

    @ApiOperation(value = "Reporte de demanda de transporte", produces = MediaType.APPLICATION_JSON_VALUE, response = ResponseReportList.class)
    @GetMapping(value = "/demanda", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDemandList> obtainDemand(
            @RequestParam(name = "puntoTransporte", required=false) String transportationPoint,
            @RequestParam(name = "fechaTransporte", required=false) String transportationDate
    ) {
        return new ResponseEntity<>(this.reportBusiness.obtainDemand( transportationPoint, transportationDate ), HttpStatus.OK);
    }

    @ApiOperation(value = "Reporte de demanda de transporte por empresa", produces = MediaType.APPLICATION_JSON_VALUE, response = ResponseReportList.class)
    @GetMapping(value = "/demanda/{nitEmpresa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDemandList> obtainDemandForBusiness(
            @PathVariable(name = "nitEmpresa") String nitBusiness,
            @RequestParam(name = "puntoTransporte", required=false) String transportationPoint,
            @RequestParam(name = "fechaTransporte", required=false) String transportationDate
    ) {
        return new ResponseEntity<>(this.reportBusiness.obtainDemand( nitBusiness, transportationPoint, transportationDate ), HttpStatus.OK);
    }
}

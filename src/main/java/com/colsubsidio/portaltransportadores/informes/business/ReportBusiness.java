package com.colsubsidio.portaltransportadores.informes.business;

import com.colsubsidio.portaltransportadores.informes.commons.documents.Reservation;
import com.colsubsidio.portaltransportadores.informes.commons.dto.ResponseDemandList;
import com.colsubsidio.portaltransportadores.informes.commons.dto.ResponseReportList;
import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.DemandDto;
import com.colsubsidio.portaltransportadores.informes.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.informes.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.informes.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.repository.DemandBusinessRepository;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.repository.DemandRepository;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.repository.TransactionRepository;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.services.ReservationService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.List;

@Component
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ReportBusiness {

    private final @NonNull DemandRepository demandRepository;
    private final @NonNull TransactionRepository transactionRepository;
    private final @NonNull DemandBusinessRepository demandBusinessRepository;
    private final @NonNull ResponseHandler responseHandler;
    private final @NonNull ReservationService reservationService;

    public ResponseReportList obtain(
            String nitBusiness,
            String transportationPoint,
            String initialDate,
            String finalDate) {
        try {
            List<Reservation> iHistory = validateFilters(nitBusiness, transportationPoint, initialDate, finalDate);
            this.responseHandler.response(HttpStatus.OK, "Historial de transporte listados exitosamente");
            return new ResponseReportList(
                    this.responseHandler.response().getResultado(),
                    this.reservationService.obtainReservation(iHistory));
        } catch (Exception e) {
            this.responseHandler.response(HttpStatus.MULTI_STATUS,
                    "Se produjo un fallo al momento de listar el historial de transporte");
            return new ResponseReportList(this.responseHandler.response().getResultado());
        }
    }

    public ResponseReportList obtain(
            String nitBusiness,
            String initialDate,
            String finalDate) {
        try {
            List<Reservation> iHistory = validateFilters(nitBusiness, null, initialDate, finalDate);
            this.responseHandler.response(HttpStatus.OK, "Historial de transporte listados exitosamente");
            return new ResponseReportList(
                    this.responseHandler.response().getResultado(),
                    this.reservationService.obtainReservation(iHistory));
        } catch (Exception e) {
            this.responseHandler.response(HttpStatus.MULTI_STATUS,
                    "Se produjo un fallo al momento de listar el historial de transporte");
            return new ResponseReportList(this.responseHandler.response().getResultado());
        }
    }

    public ResponseDemandList obtainDemand(String transportationPoint, String transportationDate) {
        try {
            List<DemandDto> demandList = validateFilter(transportationPoint, transportationDate);
            this.responseHandler.response(HttpStatus.OK, "Historial de transporte listados exitosamente");
            return new ResponseDemandList(
                    this.responseHandler.response().getResultado(),
                    demandList);
        } catch (Exception e) {
            this.responseHandler.response(HttpStatus.MULTI_STATUS,
                    "Se produjo un fallo al momento de listar el historial de transporte");
            return new ResponseDemandList(this.responseHandler.response().getResultado());
        }
    }

    public ResponseDemandList obtainDemand(String nitBusiness, String transportationPoint, String transportationDate) {
        try {
            List<DemandDto> demandList = validateFilter(nitBusiness, transportationPoint, transportationDate);
            this.responseHandler.response(HttpStatus.OK, "Historial de transporte listados exitosamente");
            return new ResponseDemandList(
                    this.responseHandler.response().getResultado(),
                    demandList);
        } catch (Exception e) {
            this.responseHandler.response(HttpStatus.MULTI_STATUS,
                    "Se produjo un fallo al momento de listar el historial de transporte");
            return new ResponseDemandList(this.responseHandler.response().getResultado());
        }
    }

    private List<DemandDto> validateFilter(String nitBusiness, String transportationPoint, String transportationDate) {
        if (StringUtils.hasLength(transportationDate)) {
            validateDate(transportationDate);
            if (StringUtils.hasLength(transportationPoint))
                return this.demandBusinessRepository.getDemandDto(nitBusiness, transportationPoint, transportationDate);
            return this.demandBusinessRepository.getDemandDtoByReservationDate(nitBusiness, transportationDate);
        }
        if (StringUtils.hasLength(transportationPoint))
            return this.demandBusinessRepository.getDemandDto(nitBusiness, transportationPoint);
        return this.demandBusinessRepository.getDemandDto(nitBusiness);
    }

    private List<DemandDto> validateFilter(String transportationPoint, String transportationDate) {
        if (StringUtils.hasLength(transportationDate)) {
            validateDate(transportationDate);
            if (StringUtils.hasLength(transportationPoint))
                return this.demandRepository.getDemandDto(transportationPoint, transportationDate);
            return this.demandRepository.getDemandDtoByReservationDate(transportationDate);
        }
        if (StringUtils.hasLength(transportationPoint))
            return this.demandRepository.getDemandDto(transportationPoint);
        return this.demandRepository.getDemandDto();
    }

    private List<Reservation> validateFilters(
            String nitBusiness,
            String transportationPoint,
            String initialDate,
            String finalDate) {

        if (StringUtils.hasLength(initialDate) && StringUtils.hasLength(finalDate)) {
            validateDate(initialDate, finalDate);
            return this.transactionRepository.getHistory(nitBusiness, transportationPoint, initialDate, finalDate);
        }
        return this.transactionRepository.getHistory(nitBusiness,
                transportationPoint,
                DateUtils.firstDayOfMonth(),
                DateUtils.lastDayOfMonth());
    }

    private void validateDate(String date) {
        if (!DateUtils.isValid(date, EDateFormat.ISO_8601_SHORT.getFormat()))
            this.responseHandler.response(HttpStatus.BAD_REQUEST, "El valor de 'fechaTransporte' no es valido");
    }

    private void validateDate(String initialDate,
                              String finalDate) {
        if (!DateUtils.isValid(initialDate, EDateFormat.ISO_8601_SHORT.getFormat()))
            this.responseHandler.response(HttpStatus.BAD_REQUEST, "El valor de 'fechaInicial' no es valido");
        if (!DateUtils.isValid(finalDate, EDateFormat.ISO_8601_SHORT.getFormat()))
            this.responseHandler.response(HttpStatus.BAD_REQUEST, "El valor de 'fechaFinal' no es valido");
    }
}

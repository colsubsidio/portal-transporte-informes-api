package com.colsubsidio.portaltransportadores.informes.integrations.dao;

import com.colsubsidio.portaltransportadores.informes.commons.documents.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PointDao extends MongoRepository<Point, String> {
    Point findByRowKey(String rowKey);
}

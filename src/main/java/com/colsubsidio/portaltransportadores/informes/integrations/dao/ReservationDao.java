package com.colsubsidio.portaltransportadores.informes.integrations.dao;

import com.colsubsidio.portaltransportadores.informes.commons.documents.Reservation;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ReservationDao extends MongoRepository<Reservation, String> {
    List<Reservation> findByFechaEliminado( String deleteDate );
    List<Reservation> findByFechaReservaBetween( String initDate, String lastDate );
    List<Reservation> findByFechaReservaBetweenAndNitEmpresa( String initDate, String lastDate, String nitBusiness );
    List<Reservation> findByFechaReservaBetweenAndPartitionKey( String initDate, String lastDate, String partitionKey );
    List<Reservation> findByFechaReservaBetweenAndNitEmpresaAndPartitionKey( String initDate, String lastDate, String nitBusiness, String partitionKey );
    List<Reservation> findByFechaEliminadoAndFechaReserva( String deleteDate, String dateReservation );
    List<Reservation> findByFechaEliminadoAndNitEmpresa( String deleteDate, String nitBusiness );
    List<Reservation> findByFechaEliminadoAndPartitionKey( String deleteDate, String partitionKey );
    List<Reservation> findByFechaEliminadoAndNitEmpresaAndPartitionKey( String deleteDate, String nitBusiness, String partitionKey );
    List<Reservation> findByFechaEliminadoAndNitEmpresaAndFechaReserva( String deleteDate, String nitBusiness, String dateReservation );
    List<Reservation> findByFechaEliminadoAndNitEmpresaAndPartitionKeyAndFechaReserva( String deleteDate, String nitBusiness, String partitionKey, String dateReservation );
    List<Reservation> findByFechaEliminadoAndPartitionKeyAndFechaReserva( String deleteDate, String partitionKey, String dateReservation );

}

package com.colsubsidio.portaltransportadores.informes.integrations.dao.services;

import com.colsubsidio.portaltransportadores.informes.commons.documents.Point;
import com.colsubsidio.portaltransportadores.informes.commons.documents.Reservation;
import com.colsubsidio.portaltransportadores.informes.commons.dto.TransportDto;
import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.ClientRespDto;
import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.PersonaDto;
import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.ReservationDto;
import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.ReservationRespDto;
import com.colsubsidio.portaltransportadores.informes.commons.dto.tickets.BoletumDto;
import com.colsubsidio.portaltransportadores.informes.commons.dto.tickets.TicketDto;
import com.colsubsidio.portaltransportadores.informes.commons.dto.tickets.TicketRespDto;

import com.colsubsidio.portaltransportadores.informes.commons.utilities.RestTemplateUtil;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.PointDao;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.repository.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class ReservationService {

    private final @NonNull RestTemplateUtil restTemplateUtil;
    private final @NonNull UserRepository userRepository;
    private final @NonNull PointDao pointDao;

    @Value("${colsubsidio.reservation.url-tickets}")
    private String urlTickets;

    @Value("${colsubsidio.reservation.url-reservation}")
    private String urlReservation;


    private List<BoletumDto> obtainTicketByIdReservationAndDocumentNumber(String idReservation, String documentNumber) {

        //UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(urlTickets.replace("{documentNumber}", documentNumber));
        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(urlTickets);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        ResponseEntity<TicketDto> ticketDto = restTemplateUtil.sendRequest(uri, HttpMethod.GET, null, TicketDto.class, false, headers);

        return Objects.requireNonNull(ticketDto.getBody()).getBody().getObtenerEstadoBoletaUsuario().get(0)
                .getBoleta()
                .stream()
                .filter(ticket -> Integer.toString(ticket.getReserva().getCodigo()).equals(idReservation))
                .collect(Collectors.toList());
    }

    private ReservationDto obtainReservationByIdReservation(String idReservation) {
        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(urlReservation);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        ResponseEntity<ReservationDto> ticketDto = restTemplateUtil.sendRequest(uri, HttpMethod.GET, null, ReservationDto.class, false, headers);
        return Objects.requireNonNull(ticketDto.getBody());
    }

    public List<ReservationRespDto> obtainReservation(List<Reservation> reservationEntityList) {

        List<ReservationRespDto> reservationRespDtoList = new ArrayList<>();
        for (Reservation reservationEntity : reservationEntityList) {
            ReservationRespDto reservationRespDto = new ReservationRespDto();
            reservationRespDto.setReserva(getTicketRespDto(reservationEntity));
            reservationRespDto.setCliente(getClientRespDto(reservationEntity.getIdReserva()));
            reservationRespDtoList.add(reservationRespDto);
        }

        return reservationRespDtoList;
    }


    private TicketRespDto getTicketRespDto(Reservation reservationEntity) {
        List<BoletumDto> boletumDtoList = obtainTicketByIdReservationAndDocumentNumber(
                reservationEntity.getIdReserva(),
                reservationEntity.getNumeroDocumentoCliente()
        );
        TicketRespDto ticketRespDto = new TicketRespDto();
        ticketRespDto.setTransporte(getTransportDto(reservationEntity));
        if (!boletumDtoList.isEmpty()) {
            ticketRespDto.setCodigo(boletumDtoList.get(0).getCodigo());
            ticketRespDto.setDetalle(boletumDtoList.get(0).getProducto().getDescripcion());
            ticketRespDto.setFecha(reservationEntity.getFechaReserva());
        }
        return ticketRespDto;
    }

    private TransportDto getTransportDto(Reservation reservationEntity) {
        Point point = this.pointDao.findByRowKey(reservationEntity.getPartitionKey());

        TransportDto transportDto = new TransportDto();
        transportDto.setIdReserva(reservationEntity.getIdReserva());
        transportDto.setDocumentoConductor(reservationEntity.getNumeroDocumentoConductor());
        transportDto.setFechaCreado(reservationEntity.getFechaCreado());
        transportDto.setFechaEliminado(reservationEntity.getFechaEliminado());
        transportDto.setFechaReserva(reservationEntity.getFechaReserva());
        transportDto.setFechaIda(reservationEntity.getFechaIda());
        transportDto.setFechaRegreso(reservationEntity.getFechaRegreso());
        transportDto.setValor(reservationEntity.getValor());
        if (StringUtils.hasLength(reservationEntity.getNumeroDocumentoConductor()))
            transportDto.setTransportador(this.userRepository.getUsersDocumentNumber(reservationEntity.getNumeroDocumentoConductor()));
        if (StringUtils.hasLength(reservationEntity.getNitEmpresa()))
            transportDto.setTransportadora(this.userRepository.getUsersNitBusiness(reservationEntity.getNitEmpresa()));
        if (!ObjectUtils.isEmpty(point))
            transportDto.setPuntoRecogida(point.getNombrePunto());
        return transportDto;
    }

    private ClientRespDto getClientRespDto(String idReservation) {
        ReservationDto reservationDto = obtainReservationByIdReservation(idReservation);
        if (!ObjectUtils.isEmpty(reservationDto)) {
            PersonaDto person = reservationDto.getBody().getObtenerReserva().get(0).getPersona();
            return new ClientRespDto(
                    person.getPrimerApellido(),
                    person.getPrimerNombre(),
                    person.getSegundoApellido(),
                    person.getSegundoNombre(),
                    person.getIdentificacion().getCodigo());
        } else {
            return new ClientRespDto();
        }
    }
}

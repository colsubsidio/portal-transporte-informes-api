package com.colsubsidio.portaltransportadores.informes.integrations.dao.repository;

import com.colsubsidio.portaltransportadores.informes.commons.documents.Reservation;
import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.DemandDto;
import com.colsubsidio.portaltransportadores.informes.commons.utilities.TableStorageUtils;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.ReservationDao;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor(onConstructor = @__(@Inject))
public class DemandBusinessRepository {

    private final @NonNull ReservationDao reservationDao;

    public List<DemandDto> getDemandDto(String nitBusiness) {
        List<Reservation> historyList = this.reservationDao
                .findByFechaEliminadoAndNitEmpresa("", nitBusiness);
        return TableStorageUtils.getDemandDto(historyList);
    }

    public List<DemandDto> getDemandDto(String nitBusiness, String transportationPoint) {
        List<Reservation> listEntity = this.reservationDao
                .findByFechaEliminadoAndNitEmpresaAndPartitionKey("", nitBusiness, transportationPoint);
        List<DemandDto> demandDtoList = new ArrayList<>();

        List<String> datesReservation = listEntity.stream().map(Reservation::getFechaReserva).distinct().collect(Collectors.toList());
        TableStorageUtils.generateDemandDtoList(demandDtoList, listEntity, datesReservation);

        return demandDtoList;
    }

    public List<DemandDto> getDemandDtoByReservationDate(String nitBusiness, String transportationDate) {
        List<Reservation> historyList = this.reservationDao
                .findByFechaEliminadoAndNitEmpresaAndFechaReserva("", nitBusiness, transportationDate);
        return TableStorageUtils.getDemandDtoByReservationDate(historyList);
    }

    public List<DemandDto> getDemandDto(String nitBusiness, String transportationPoint, String transportationDate) {
        List<Reservation> listReservation = this.reservationDao
                .findByFechaEliminadoAndNitEmpresaAndPartitionKeyAndFechaReserva("", nitBusiness, transportationPoint, transportationDate);
        List<DemandDto> demandDtoList = new ArrayList<>();
        TableStorageUtils.generateDemand(listReservation, demandDtoList);
        return demandDtoList;
    }

}

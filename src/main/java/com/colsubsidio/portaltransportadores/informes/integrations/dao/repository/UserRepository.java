package com.colsubsidio.portaltransportadores.informes.integrations.dao.repository;

import com.colsubsidio.portaltransportadores.informes.commons.utilities.CarrierDtoUtils;
import com.colsubsidio.portaltransportadores.informes.commons.utilities.ConveyorDtoUtils;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.UserDao;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class UserRepository {

    private final @NonNull UserDao userDao;

    public CarrierDtoUtils getUsersDocumentNumber(String documentNumber) {
        return new CarrierDtoUtils(this.userDao.findByNumeroDocumento(documentNumber));
    }

    public ConveyorDtoUtils getUsersNitBusiness(String documentNumber) {
        return new ConveyorDtoUtils(this.userDao.findByNumeroDocumento(documentNumber));
    }

}

package com.colsubsidio.portaltransportadores.informes.integrations.dao;

import com.colsubsidio.portaltransportadores.informes.commons.documents.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserDao extends MongoRepository<User, String> {
    User findByNumeroDocumento(String documentNumber);
}

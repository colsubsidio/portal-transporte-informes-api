package com.colsubsidio.portaltransportadores.informes.integrations.dao.repository;

import com.colsubsidio.portaltransportadores.informes.commons.documents.Reservation;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.ReservationDao;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import javax.inject.Inject;
import java.util.List;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class TransactionRepository {

    private final @NonNull ReservationDao reservationDao;

    public List<Reservation> getHistory(String idBusiness,
                                        String transportationPoint,
                                        String initialDate,
                                        String finalDate) {
        if (!ObjectUtils.isEmpty(idBusiness)) {
            if (!ObjectUtils.isEmpty(transportationPoint))
                return this.reservationDao
                        .findByFechaReservaBetweenAndNitEmpresaAndPartitionKey(initialDate, finalDate, idBusiness, transportationPoint);
            return this.reservationDao.findByFechaReservaBetweenAndNitEmpresa(initialDate, finalDate, idBusiness);
        }
        if (!ObjectUtils.isEmpty(transportationPoint))
            return this.reservationDao.findByFechaReservaBetweenAndPartitionKey(initialDate, finalDate, transportationPoint);
        return this.reservationDao.findByFechaReservaBetween(initialDate, finalDate);
    }
}

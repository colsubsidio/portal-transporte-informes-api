package com.colsubsidio.portaltransportadores.informes.commons.exeptions;

public class BusinessException extends Exception {

    private static final long serialVersionUID = -2425789308787919687L;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

}
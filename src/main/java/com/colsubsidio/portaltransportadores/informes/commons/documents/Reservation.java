package com.colsubsidio.portaltransportadores.informes.commons.documents;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "reservation")
public class Reservation implements Serializable {

    private static final long serialVersionUID = 6949325488891848546L;

    @Id
    private String id;

    private String partitionKey;
    private String rowKey;
    private String idReserva;
    private String valor;
    private String numeroDocumentoCliente;
    private String numeroDocumentoConductor;
    private String fechaReserva;
    private String fechaCreado;
    private String fechaIda;
    private String fechaRegreso;
    private String fechaEliminado;
    private String nitEmpresa;
}

package com.colsubsidio.portaltransportadores.informes.commons.utilities;

import com.colsubsidio.portaltransportadores.informes.commons.documents.User;
import com.colsubsidio.portaltransportadores.informes.commons.dto.CarrierDto;

import java.io.Serializable;

public class CarrierDtoUtils extends CarrierDto implements Serializable {
    private static final long serialVersionUID = -5191140736926498627L;

    public CarrierDtoUtils(User userEntity) {
        super(userEntity.getNombreCompleto(), userEntity.getNumeroDocumento());
    }
}

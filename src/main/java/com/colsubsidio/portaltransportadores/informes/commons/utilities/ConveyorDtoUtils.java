package com.colsubsidio.portaltransportadores.informes.commons.utilities;

import com.colsubsidio.portaltransportadores.informes.commons.documents.User;
import com.colsubsidio.portaltransportadores.informes.commons.dto.ConveyorDto;

import java.io.Serializable;

public class ConveyorDtoUtils extends ConveyorDto implements Serializable {

    private static final long serialVersionUID = -9017610629070336940L;

    public ConveyorDtoUtils(User userEntity) {
        super(userEntity.getNombreCompleto(), userEntity.getNumeroDocumento());
    }
}

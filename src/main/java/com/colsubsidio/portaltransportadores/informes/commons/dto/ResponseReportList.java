package com.colsubsidio.portaltransportadores.informes.commons.dto;

import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.ReservationRespDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ResponseReportList extends ResponseDto implements Serializable {
    private static final long serialVersionUID = -5278440485657754953L;
    private List<ReservationRespDto> data;

    public ResponseReportList(List<ResultDto> resultado, List<ReservationRespDto> data) {
        super(resultado);
        this.data = data;
    }

    public ResponseReportList(List<ResultDto> resultado) {
        super(resultado);
    }
}

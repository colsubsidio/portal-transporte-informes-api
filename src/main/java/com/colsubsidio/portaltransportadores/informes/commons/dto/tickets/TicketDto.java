package com.colsubsidio.portaltransportadores.informes.commons.dto.tickets;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TicketDto {
    private String code;
    private BodyTicketDto body;
    private Boolean success;
    private String message;
}

package com.colsubsidio.portaltransportadores.informes.commons.dto.tickets;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BodyTicketDto {
    private List<ResultadoDto> resultado = null;
    private List<ObtenerEstadoBoletaUsuarioDto> obtenerEstadoBoletaUsuario = null;
}

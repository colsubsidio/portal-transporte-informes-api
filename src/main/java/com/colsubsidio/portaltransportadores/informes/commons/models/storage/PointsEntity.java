package com.colsubsidio.portaltransportadores.informes.commons.models.storage;

import com.microsoft.azure.storage.table.TableServiceEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PointsEntity extends TableServiceEntity implements Serializable {

    private static final long serialVersionUID = -3773095471779422224L;
    private String nombrePunto;
    private String direccion;
    private String ubicacionMapa;
    private String telefono;
    private String horarioSalida;
    private String recomendaciones;
    private String nitEmpresa;
    private String fechaCreado;
    private String fechaActualizado;
    private String fechaEliminado;
}

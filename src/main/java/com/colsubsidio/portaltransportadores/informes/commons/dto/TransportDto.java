package com.colsubsidio.portaltransportadores.informes.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TransportDto implements Serializable {
    private static final long serialVersionUID = -9044069187614689539L;

    private String idReserva;
    private String valor;
    private String documentoConductor;
    private String fechaReserva;
    private String fechaCreado;
    private String fechaIda;
    private String fechaRegreso;
    private String fechaEliminado;
    private CarrierDto transportador;
    private ConveyorDto transportadora;
    private String puntoRecogida;
}

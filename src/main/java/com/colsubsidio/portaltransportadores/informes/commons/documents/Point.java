package com.colsubsidio.portaltransportadores.informes.commons.documents;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "point")
public class Point implements Serializable {

    private static final long serialVersionUID = -1351256986045793591L;

    @Id
    private String id;

    private String partitionKey;
    private String rowKey;
    private String nombrePunto;
    private String direccion;
    private String ubicacionMapa;
    private String telefono;
    private String horarioSalida;
    private String recomendaciones;
    private String nitEmpresa;
    private String fechaCreado;
    private String fechaActualizado;
    private String fechaEliminado;
}

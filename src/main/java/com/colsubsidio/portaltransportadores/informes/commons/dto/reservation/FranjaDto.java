package com.colsubsidio.portaltransportadores.informes.commons.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FranjaDto {

    private int id;
    private String nombre;
}

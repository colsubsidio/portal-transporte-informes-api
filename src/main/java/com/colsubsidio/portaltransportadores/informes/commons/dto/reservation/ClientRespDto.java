package com.colsubsidio.portaltransportadores.informes.commons.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ClientRespDto implements Serializable {
    private static final long serialVersionUID = -425620332972094156L;
    private String primerApellido;
    private String primerNombre;
    private String segundoApellido;
    private String segundoNombre;
    private String numDoc;
}

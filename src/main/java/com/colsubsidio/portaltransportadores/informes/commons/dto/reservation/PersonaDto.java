package com.colsubsidio.portaltransportadores.informes.commons.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDto {

    private String celular;
    private String correo;
    private IdentificacionDto identificacion;
    private String primerApellido;
    private String primerNombre;
    private String segundoApellido;
    private String segundoNombre;
    private String telefono;
}

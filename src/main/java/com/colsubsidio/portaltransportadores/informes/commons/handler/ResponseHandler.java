package com.colsubsidio.portaltransportadores.informes.commons.handler;

import com.colsubsidio.portaltransportadores.informes.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.informes.commons.dto.ResultDto;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.ArrayList;
import java.util.List;

@Component
public class ResponseHandler {

    private ResponseDto responseDto;

    {
        responseDto = new ResponseDto();
    }

    public void response(HttpStatus httpStatus, String... descriptions) throws HttpStatusCodeException {

        for (String description : descriptions) {
            ResultDto result = new ResultDto();
            result.setCodigo(httpStatus.value());
            result.setDescripcion(description);

            List<ResultDto> responseDto = this.responseDto.getResultado();
            if (ObjectUtils.isEmpty(responseDto)) {
                List<ResultDto> response = new ArrayList<>();
                response.add(result);
                this.responseDto.setResultado(response);
            } else {
                responseDto.add(result);
            }
        }

        if (httpStatus.series().value() >= HttpStatus.Series.CLIENT_ERROR.value()) {
            throw new HttpClientErrorException(httpStatus);
        }
    }

    public ResponseDto response() {

        ResponseDto responseDto = new ResponseDto();
        responseDto.setResultado(this.responseDto.getResultado());
        this.responseDto = new ResponseDto();
        return responseDto;
    }

}


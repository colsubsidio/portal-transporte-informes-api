package com.colsubsidio.portaltransportadores.informes.commons.utilities;

import com.colsubsidio.portaltransportadores.informes.commons.enums.EDateFormat;
import org.apache.commons.validator.DateValidator;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class DateUtils extends DateValidator {

    public static String getDateString(String formatReturn) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatReturn);
        return sdf.format(new Date());
    }

    public static boolean isValid(String dateStr, String formatReturn) {
        DateFormat sdf = new SimpleDateFormat( formatReturn );
        sdf.setLenient(false);
        try {
            sdf.parse(dateStr);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static boolean isBeforeCurrent(String dateStr, String formatReturn) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatReturn);
        sdf.setLenient(false);
        try {
            Date date = sdf.parse(dateStr);
            Date currentDate = sdf.parse(getDateString(formatReturn));
            return date.before(currentDate);
        } catch (ParseException e) {
            return true;
        }
    }

    public static boolean isEqualsCurrent(String dateStr, String formatReturn) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatReturn);
        sdf.setLenient(false);
        try {
            Date date = sdf.parse(dateStr);
            Date currentDate = sdf.parse(getDateString(formatReturn));
            return date.equals(currentDate);
        } catch (ParseException e) {
            return false;
        }
    }

    public static String lastDayOfMonth() {
        Calendar calendar = Calendar.getInstance();
        return getDateString( EDateFormat.ISO_8601_MEDIUM.getFormat() ) + "-" +
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static String firstDayOfMonth() {
        Calendar calendar = Calendar.getInstance();
        return getDateString( EDateFormat.ISO_8601_MEDIUM.getFormat() ) + "-01";
    }

}

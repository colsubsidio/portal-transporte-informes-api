package com.colsubsidio.portaltransportadores.informes.commons.dto;

import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.DemandDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ResponseDemandList extends ResponseDto implements Serializable {
    private static final long serialVersionUID = 8659333004861995538L;
    private List<DemandDto> data;

    public ResponseDemandList(List<ResultDto> resultado) {
        super(resultado);
    }

    public ResponseDemandList(List<ResultDto> resultado, List<DemandDto> data) {
        super(resultado);
        this.data = data;
    }
}

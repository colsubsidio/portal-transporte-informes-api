package com.colsubsidio.portaltransportadores.informes.commons.utilities;

import com.colsubsidio.portaltransportadores.informes.commons.documents.Point;
import com.colsubsidio.portaltransportadores.informes.commons.documents.Reservation;
import com.colsubsidio.portaltransportadores.informes.commons.dto.reservation.DemandDto;
import com.colsubsidio.portaltransportadores.informes.integrations.dao.PointDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TableStorageUtils {

    private static PointDao POINTSREPOSITORY;

    @Autowired
    public TableStorageUtils(PointDao pointsRepository) {
        TableStorageUtils.POINTSREPOSITORY = pointsRepository;
    }

    public static void generateDemand(List<Reservation> listReservation, List<DemandDto> demandDtoList) {
        Point pointsEntity = POINTSREPOSITORY.findByRowKey(listReservation.get(0).getPartitionKey());
        DemandDto demandDto = new DemandDto();
        demandDto.setTotalPersonas(listReservation.size());
        demandDto.setFechaTransporte(listReservation.get(0).getFechaReserva());
        if (!ObjectUtils.isEmpty(pointsEntity)) demandDto.setPuntoRecogida(pointsEntity.getNombrePunto());
        else demandDto.setPuntoRecogida("");
        demandDtoList.add(demandDto);
    }

    public static List<DemandDto> getDemandDto(List<Reservation> historyList) {
        List<DemandDto> demandDtoList = new ArrayList<>();
        List<String> points = historyList.stream().map(Reservation::getPartitionKey).distinct().collect(Collectors.toList());
        for (String point : points) {
            List<Reservation> listEntity = historyList.stream().filter(entity -> entity.getPartitionKey().equals(point)).collect(Collectors.toList());
            List<String> datesReservation = listEntity.stream().map(Reservation::getFechaReserva).distinct().collect(Collectors.toList());
            generateDemandDtoList(demandDtoList, listEntity, datesReservation);
        }
        return demandDtoList;
    }

    public static List<DemandDto> getDemandDtoByReservationDate(List<Reservation> historyList) {
        List<DemandDto> demandDtoList = new ArrayList<>();
        List<String> points = historyList.stream().map(Reservation::getPartitionKey).distinct().collect(Collectors.toList());
        for (String point : points) {
            List<Reservation> listEntity = historyList.stream().filter(entity -> entity.getPartitionKey().equals(point)).collect(Collectors.toList());
            generateDemand(listEntity, demandDtoList);
        }
        return demandDtoList;
    }

    public static void generateDemandDtoList(List<DemandDto> demandDtoList, List<Reservation> listEntity, List<String> datesReservation) {
        for (String dateReservation : datesReservation) {
            List<Reservation> listReservation = listEntity.stream().filter(entity -> entity.getFechaReserva().equals(dateReservation)).collect(Collectors.toList());
            generateDemand(listReservation, demandDtoList);
        }
    }

}

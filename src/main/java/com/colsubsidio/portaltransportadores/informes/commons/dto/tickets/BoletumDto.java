package com.colsubsidio.portaltransportadores.informes.commons.dto.tickets;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BoletumDto {
    private String codigo;
    private String tipo;
    private String tarifa;
    private ReservaDto reserva;
    private String fechaCompra;
    private String fechaVencimiento;
    private String estado;
    private ProductoDto producto;
    private CentroServicioDto centroServicio;
}

package com.colsubsidio.portaltransportadores.informes.commons.dto.reservation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class DemandDto implements Serializable {

    private static final long serialVersionUID = -8643142740516405734L;
    private String fechaTransporte;
    private String puntoRecogida;
    private int totalPersonas;

}

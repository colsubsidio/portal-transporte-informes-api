package com.colsubsidio.portaltransportadores.informes.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ConveyorDto implements Serializable {
    private static final long serialVersionUID = 3645142015692494634L;
    private String nombreCompleto;
    private String nit;
}

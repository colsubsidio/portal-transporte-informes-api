package com.colsubsidio.portaltransportadores.informes.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CarrierDto implements Serializable {
    private static final long serialVersionUID = 2467479664258454186L;
    private String nombreCompleto;
    private String numDoc;
}

package com.colsubsidio.portaltransportadores.informes.commons.dto.tickets;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReservaDto {
    private int codigo;
    private String fecha;
}

package com.colsubsidio.portaltransportadores.informes.commons.models.storage;

import com.microsoft.azure.storage.table.TableServiceEntity;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserEntity extends TableServiceEntity implements Serializable {

    private static final long serialVersionUID = -3773095471779422224L;

    private String usuario;
    private String numeroDocumento;
    private String tipoDocumento;
    private String clave;
    private String correo;
    private String nombreCompleto;
    private String estado;
    private String perfil;
    private String nitEmpresa;
    private String fechaCreado;
    private String fechaActualizado;
    private String fechaEliminado;

}

package com.colsubsidio.portaltransportadores.informes.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.logging.log4j.Level;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public enum ELevelTelemetry {
    TRACE(HttpStatus.OK, Level.TRACE),
    DEBUG(HttpStatus.OK, Level.DEBUG),
    INFO(HttpStatus.OK, Level.INFO),
    WARN(HttpStatus.INTERNAL_SERVER_ERROR, Level.WARN),
    ERROR(HttpStatus.INTERNAL_SERVER_ERROR, Level.ERROR);

    private final HttpStatus httpStatus;
    private final Level level;

}

package com.colsubsidio.portaltransportadores.informes.commons.dto.tickets;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ObtenerEstadoBoletaUsuarioDto {
    private Boolean operacionExitosa;
    private String mensajeOriginal;
    private List<BoletumDto> boleta = null;
}
